#!/usr/bin/env make

# -*- mode: makefile-gmake -*-

all: build web

build: atc fly

atc:
	build/atc

fly:
	build/fly

web: public templates

public:
	build/public

templates:
	build/templates
