# concourse-concourse

Concourse Pipeline for Concourse on Docker

# Usage
```shell
fly set-pipeline --pipeline concourse --config pipeline.yaml
```

# License
[MIT](https://tldrlegal.com/license/mit-license)

# Contributors
  * [Chris Olstrom](https://colstrom.github.io/) | [e-mail](mailto:chris@olstrom.com) | [Twitter](https://twitter.com/ChrisOlstrom)
