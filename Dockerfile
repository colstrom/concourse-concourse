FROM colstrom/alpine

COPY atc /opt/atc
COPY run/start /usr/local/sbin/
ADD https://gist.githubusercontent.com/colstrom/b357296739713e7d83e2/raw/f52430546934b2b7fcadca40f22b8aa25e12e0cf/container-ip.sh /usr/local/bin/container-ip
RUN chmod +x /usr/local/bin/container-ip

WORKDIR /opt/atc

EXPOSE 8080

ENTRYPOINT ["start"]
